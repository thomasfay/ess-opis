# ESS CS-Studio OPIs

This folder contains the validated and approved CS-Studio OPI files to be used
at ESS to control the facility.

## Structure

Here the structure of this folder:

```
ess-opis/
├── COMMON
│   ├── DEVICES
│   └── UTILITIES
├── CONTROL-ROOM
│   ├── accelerator
│   ├── central-services
│   ├── COMMON
│   ├── machine-protection-system
│   ├── neutron-instruments
│   ├── personnel-safety-systems
│   └── target-station
├── ENGINEERING
│   ├── accelerator
│   ├── central-services
│   ├── COMMON
│   ├── machine-protection-system
│   ├── neutron-instruments
│   ├── personnel-safety-systems
│   └── target-station
└── NON-APPROVED
```

The NON-APPROVED directory is used for OPI files that don't meet the standard yet, but
that need to be deployed for testing purpose.

Each COMMON folder should be used to contain OPIs common to that level.
The folder has the following structure:

```
COMMON/
├── DEVICES
│   ├── beam-magnets-and-deflector
│   ├── control-system
│   ├── conventional-power
│   ├── cryogenics
│   ├── electromagnetic-resonator
│   ├── general-mechanics
│   ├── heating-cooling-and-air-conditioning
│   ├── intercepting-devices
│   ├── ion-source-specialties
│   ├── machine-protection-systems
│   ├── motion-control
│   ├── network-equipment
│   ├── neutron-chopper
│   ├── odh-detection-systems
│   ├── personnel-safety-systems
│   ├── power-convertors
│   ├── proton-beam-instrumentation
│   ├── rf-systems
│   ├── sample-environment-equipment
│   ├── sample-environment-systems
│   ├── timing-system
│   ├── vacuum
│   └── water-cooling
└── UTILITIES
```

Here the structure for the remaining folders in control-room and engineering:

```
accelerator/
├── accelerator-to-target
├── drift-tube-linac
├── dumpline
├── front-end-building
├── high-beta-linac
├── high-energy-beam-transport
├── ion-source
├── low-energy-beam-transport
├── medium-beta-linac
├── medium-energy-beam-transport
├── radio-frequency-quadrupole
├── spoke-linac
├── stubs
├── test-stand-2
└── test-stand-3

central-services/
├── argon-gas-system
├── conventional-ventilation-system
├── conventional-waste-water-system
├── cooling-water-high-system
├── cooling-water-low-system
├── cooling-water-medium-system
├── cooling-water-system
├── cryo-systems
├── di-ionised-water-system
├── district-heating-low-system
├── electrical-power-system
├── heating-water-system
├── instrument-air-system
├── lab-gases-system
├── network-system
├── nitrogen-gas-system
├── non-potable-water-system
├── potable-water-system
├── process-ventilation-system
├── radiological-ventilation-system
├── radiological-waste-water-system
├── risk-waste-water-system
├── site-infrastructure
├── sprinkler-system
├── standpipe-system
├── storm-water-system
├── timing-system
└── vacuum-system

machine-protection-system/
├── common-machine-protection-functions
├── fast-beam-interlock-systems
└── local-protection-system

neutron-instruments/
├── instrument-1
└── nmx

personnel-safety-systems/
├── coldbox-building
├── ctl-gallery
├── helium-compressor-building
├── klystron-gallery
└── pss-accelerator

target-station/
├── active-cells
├── active-fluids
├── active-handling
├── active-workshops
├── handling-mock-up
├── intermediate-cooling-systems
├── mobile-active-vacuum
├── moderator-reflector
├── monolith
├── proton-beam-window
├── radioactive-gaseous-effluent-confinement
├── shielding
├── target-safety-system
├── target-systems
├── ts-storage
└── tune-up-dump
```

## File and Folder Names

- File and folder names should be in accordance with the naming conventions as
  much as possible.
- Folders named according to the naming conventions must have names in lowercase.
- Folders used to structure the repository must have names in UPPERCASE.
- All other folder names must be in CamelCase, without white spaces.
- No white spaces are allowed in file names: use '-' instead, or CamelCase.

## Contributing the Repository

The repository can be cloned by everyone interested in. Changes can be performed
only through Pull Requests.

Currently only Claudio Rosati (claudio.rosati@esss.se) and Bejamin Bertrand (for
safety, as Infrastructure Group member, benjamin.bertrand@esss.se) are allowed
to approve the pull requests, and merge them into the repository.

As soon as someone else will be appointed to be in charge of this repository,
this readme file will be updated accordingly.

## Deployment

The ess-opis repository is cloned and mounted in CS-Studio by the [Ansible role](https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-cs-studio)
used to deploy CS-Studio.
Every time a push is done on the master branch, it triggers an Ansible job via
AWX API to update the git repository on the LCR workstations. Refer to the
`.gitlab-ci.yml` file for the implementation.
