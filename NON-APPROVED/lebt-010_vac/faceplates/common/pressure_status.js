PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;

var tooltip = "$(pv_name)\n$(pv_value)";

try
{
	var pvStat   = PVUtil.getString(pvs[0]);
	var pvPrsStr = pvs[1];

	if (pvStat != "ON" && pvStat != "Open") // && pvStat != "UNDER-RANGE" && pvStat != "OVER-RANGE")
	{
		tooltip = "$(pv_name)\n" + PVUtil.getString(pvPrsStr);
	}
} catch (err)
{
}

widget.setPropertyValue("tooltip", tooltip);
