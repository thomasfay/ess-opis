importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var xArray = PVUtil.getDoubleArray(pvs[0]);
var xArrayFirst = PVUtil.getDoubleArray(pvs[1]);

var yArrayOld = PVUtil.getDoubleArray(pvs[2]);
var yArrayNew = PVUtil.getDoubleArray(pvs[3]);

var nCnt = PVUtil.getDouble(pvs[4]);
var nNORD = PVUtil.getDouble(pvs[5]);
var nNUSE = PVUtil.getDouble(pvs[6]);

var xArrayPlot = DataUtil.createDoubleArray(nCnt);
var yArrayPlot = DataUtil.createDoubleArray(nCnt);

var i=0;

/*ConsoleUtil.writeInfo(n);*/

if(nCnt == nNORD)
{
	for(i=0; i<nCnt; i++)
	{
		xArrayPlot[i] = xArray[i];
	}	
	for(i=0; i<nNUSE; i++)
	{
		yArrayPlot[i] = yArrayOld[i];
	}	
	for(i=nNUSE; i<nCnt; i++)
	{
		yArrayPlot[i] = yArrayNew[i];
	}	
} 
else
{
	for(i=0; i<nCnt; i++)
	{
		xArrayPlot[i] = xArrayFirst[i];
		yArrayPlot[i] = yArrayNew[i];
	}		
}

pvs[7].setValue(xArrayPlot);
pvs[8].setValue(yArrayPlot);