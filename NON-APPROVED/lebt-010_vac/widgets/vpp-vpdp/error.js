PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;

var errorMsg  = "";
var errorCode = 0;

if (PVUtil.getLong(pvs[0]))
	errorCode = PVUtil.getLong(pvs[1]);

switch (errorCode) {
	case 99:
		errorMsg = "Controller Error (Hardware Error)";
		break;
	case 98:
		errorMsg = "Pressure Interlock";
		break;
	case 97:
		errorMsg = "Hardware Interlock";
		break;
	case 96:
		errorMsg = "Software Interlock";
		break;
	case 95:
		errorMsg = "Circuit Breaker Tripped";
		break;
	case 49:
		errorMsg = "Controller Error (Hardware Error) - Auto Reset";
		break;
	case 48:
		errorMsg = "Pressure Interlock - Auto Reset";
		break;
	case 47:
		errorMsg = "Hardware Interlock - Auto Reset";
		break;
	case 46:
		errorMsg = "Software Interlock - Auto Reset";
		break;
	case 2:
		errorMsg = 'Pump Disconnected - Auto Reset (for Primary Pump Controller Type "Hilko Spoelstra")';
		break;
	case 1:
		errorMsg = 'Local Control - Auto Reset (for Primary Pump Controller Type "Hilko Spoelstra")';
		break;

	case 0:
		break;
	default:
		errorMsg = "Error Code: " + PVUtil.getString(pvs[1]);
		org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger().severe("Unknown error code " + pvs[1] + " : " + errorCode);
		break;
}

if (widget.getType() != "action_button")
	widget.setPropertyValue("text", errorMsg);
widget.setPropertyValue("tooltip", errorMsg);
