PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var orientation  = PVUtil.getString(pvs[0]).toUpperCase();
var str_rotation = null;
var rotation     = -1;

try {
	str_rotation = PVUtil.getString(pvs[1]);
	if (str_rotation.startsWith("$"))
		str_rotation = null;
} catch (err) {}

function hide_pipe(pipe) {
	try {
		ScriptUtil.findWidgetByName(widget, "PIPE_" + pipe).setPropertyValue("visible", "false");
	} catch (err) {
		Logger.severe("show_pipe("+ pipe +"): " + err)
	}
}

if (orientation == "HORIZONTAL" || orientation.startsWith("$")) {
//	show_pipe("HORIZONTAL");
	hide_pipe("VERTICAL");
	rotation = 0;
} else if (orientation == "VERTICAL") {
//	show_pipe("VERTICAL");
	hide_pipe("HORIZONTAL");
	rotation = 90;
} else {
	Logger.severe("Invalid orientation: " + orientation);
	rotation = 0;
}

if (str_rotation == null) {
	try {
		ScriptUtil.findWidgetByName(widget, "Symbol").setPropertyValue("rotation", rotation);
	} catch (err) {
		Logger.severe("(orientation.js): No widget with name 'Symbol': " + err);
	}
}
