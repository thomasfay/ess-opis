PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;

var tooltip  = "N/A";

var debug = widget.getEffectiveMacros().getValue("DEBUG");
if (debug) {
	debug = debug[0];
	switch (debug) {
		case '1':
		case 'Y':
		case 'y':
		case 'T':
		case 't':
			debug = true;
			break;

		default:
			debug = false;
	}
}
else
	debug = false;

if (debug)
	Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();
else {
	Logger = new Object();
	Logger.info = function() {}
	Logger.warning = function() {}
	Logger.severe = function(text) { org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger().severe(text);}
}

function log_pv(pv) {
	try {
		Logger.info(pv + ": " + PVUtil.getString(pv));
	} catch (err) {
		Logger.severe(err);
	}
}

try {
	var pvStat   = PVUtil.getString(pvs[0]);
	var pvPrsStr = PVUtil.getString(pvs[2]);

	log_pv(pvs[0]);
	log_pv(pvs[1]);
	log_pv(pvs[2]);

	if (pvStat == "Open") {
		try {
			var vtype = PVUtil.getVType(pvs[1]);

			FormatOption = org.csstudio.display.builder.model.properties.FormatOption;
			FormatOptionHandler = org.csstudio.display.builder.model.util.FormatOptionHandler;

			tooltip = FormatOptionHandler.format(vtype, FormatOption.EXPONENTIAL, 3, true);
		} catch (err) {
			var pvUnit   = PVUtil.getString(pvs[3]);
			tooltip = pvPrsStr + " " + pvUnit;
		}
	} else
		tooltip = pvPrsStr;
} catch (err) {
	Logger.severe(err);
}

widget.setPropertyValue("tooltip", tooltip);
